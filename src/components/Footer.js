import { Container, Row, Col } from "react-bootstrap"
import { FiFacebook, FiInstagram, FiTwitter, FiMail, FiTwitch } from "react-icons/fi";
import { Link } from "react-router-dom"

function Footer() {
    return (
        <footer className="py-5 mb-5 mt-5">
            <Container>
                <Row className="gap-3">
                    <Col xs={12} lg={3}>
                        <ul className="p-0 list-unstyled d-flex flex-column gap-3">
                        <li>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</li>
                        <li>binarcarrental@gmail.com</li>
                        <li>081-233-334-808</li>
                        </ul>
                    </Col>
                    <Col xs={12} lg={2}>
                        <nav className="navbar p-0 flex-column gap-3 align-items-start">
                            <Link to="/" className="nav-link p-0 fw-bold">Our Service</Link>
                            <Link to="/" className="nav-link p-0 fw-bold">Why Us</Link>
                            <Link to="/" className="nav-link p-0 fw-bold">Testimonial</Link>
                            <Link to="/" className="nav-link p-0 fw-bold">FAQ</Link>
                        </nav>
                    </Col>
                    <Col xs={12} lg={3}>
                        <p>Connect with us</p>
                        <div className="d-flex flex-wrap gap-3">
                        <div className="icon rounded-circle">
                            <FiFacebook color="white" />
                        </div>
                        <div className="icon rounded-circle">
                            <FiInstagram color="white" />
                        </div>
                        <div className="icon rounded-circle">
                            <FiTwitter color="white" />
                        </div>
                        <div className="icon rounded-circle">
                            <FiMail color="white" />
                        </div>
                        <div className="icon rounded-circle">
                            <FiTwitch color="white"/>
                        </div>
                        </div>
                    </Col>
                    <Col xs={12} lg={3}>
                        <p>Copyright Binar 2022</p>
                        <p className="fw-bold">Binar Car Rental</p>
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}

export default Footer
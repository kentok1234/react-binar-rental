import { useEffect, useState } from "react"
import { Button, Container, Nav, Navbar } from "react-bootstrap"
import { Link } from "react-router-dom"

function Header() {
    const [headerBackground, setHeaderBackground] = useState('bg-transparent')
    const [headerShadow, setHeaderShadow] = useState('')

    const headerScroll = () => {
        window.scrollY > 10 ? setHeaderBackground('bg-white') : setHeaderBackground('bg-transparent')
        window.scrollY > 10 ? setHeaderShadow('shadow') : setHeaderShadow('')
    }

    useEffect(() => {
        window.addEventListener("scroll", headerScroll)
        return () => {
            window.removeEventListener("scroll", headerScroll)
        }
    }, [])

    return (
        <header className={`${headerBackground} ${headerShadow} w-full fixed-top`}>
            <div className="container">
            <Navbar expand={"lg"}>
                <Container>
                    <Navbar.Brand as={Link} to="/">Binar Car Rental</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="navbar-collapse justify-content-end">
                        <Nav className="mb-2 mb-lg-0 gap-4">
                            <Nav.Link href="#service">Our Services</Nav.Link>
                            <li className="nav-item">
                                <a className="nav-link" href="#whyus">Why Us</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#testimonial">Testimonial</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#faq">FAQ</a>
                            </li>
                            <li className="nav-item">
                                <Button variant="success" as={Link} to="/register" className="text-white fw-bold px-3" href="#">Register</Button>
                            </li>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            </div>

            <div className="offcanvas offcanvas-end text-black" tabIndex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                <div className="offcanvas-header">
                    <h1 id="offcanvasRightLabel" className="fw-bold">BCR</h1>
                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div className="offcanvas-body">
                    <ul className="navbar-nav mb-2 mb-lg-0 gap-4">
                    <li className="nav-item">
                        <a className="nav-link" href="#service">Our Services</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#whyus">Why Us</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#testimonial">Testimonial</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#faq">FAQ</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link btn bg-lime-green400 text-white fw-bold px-3" href="#">Register</a>
                    </li>
                    </ul>
                </div>
            </div>
        </header>
    )
}

export default Header
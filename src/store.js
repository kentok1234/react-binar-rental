import {configureStore} from '@reduxjs/toolkit'
import carsReducer from './features/cars/carsSlice'
import carReducer from './features/cars/carSlice'
import orderReducer from './features/cars/orderSlice'

export default configureStore({
    reducer: {
        cars: carsReducer,
        car: carReducer,
        order: orderReducer
    }
})
import { Breadcrumb, BreadcrumbItem, Button, Row, Card, Col, Container, ListGroup, ListGroupItem, Form, FormGroup, InputGroup, Tabs } from "react-bootstrap"
import { FiArrowLeft } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { getSelectedCar } from "../features/cars/carSlice";
import { getOrder, orderAction } from "../features/cars/orderSlice";
import { FiUsers, FiSettings, FiCalendar, FiChevronDown } from "react-icons/fi";
import { useEffect, useRef, useState } from "react";
import moment from "moment";
import { Tab } from "bootstrap";


function Payment() {
    const dispatch = useDispatch()
    const breadcrumbMenu = ['Pilih Metode', 'Bayar', 'Tiket']
    const selectedCar = useSelector(getSelectedCar)
    const order = useSelector(getOrder)
    const [tabKey, setTabKey] = useState('atm')
    const [payment, setPayment] = useState('')
    const [indexBreadcrum, setIndexBread] = useState(0)
    const [expand, setExpand] = useState(false)
    const [currentTimeOrder, setCurrentTimeOrder] = useState(moment(order.date + ' ' + order.pickup_time))
    const afterDay = moment(order.date + ' ' + order.pickup_time).add(1, 'days')

    const timeBetween = moment.duration(afterDay.diff(currentTimeOrder))

    const selectPayment = (bank) => {
        setPayment(bank)
        dispatch(orderAction.updateOrder({payment: bank}))
    }
    
    const pay = () => {
        setIndexBread(indexBreadcrum + 1)
    }
    
    useEffect(() => {
        const interval = setInterval(() => {
            if (timeBetween.days() < 0) {
                clearInterval(interval)
            }
            setCurrentTimeOrder(moment())
        }, 1000)

        return () => clearInterval(interval)
    }, [])

    return (
        <>
            <section className="w-full position-relative d-flex align-items-center" style={{backgroundColor: '#F1F3FF', height: indexBreadcrum === 0 ? '266px' : '164px'}}>
                <Container className="h-full">
                    <div className=" w-full h-full d-flex align-items-center justify-content-between">
                        <Button as={Link} to={indexBreadcrum === 0 ? '/cars' : '/payment' } className="fw-bold d-flex align-items-center gap-2" variant="link" style={{textDecoration: 'none', color:'black'}}>
                            <FiArrowLeft /> {indexBreadcrum !== 0 ? order.payment + ' Transfer' : 'Pembayaran'}
                        </Button>
                        <Breadcrumb bsPrefix="breadcrumb mb-0">
                            {breadcrumbMenu.map((bread, index) => (
                                <BreadcrumbItem href="/payment" key={index} onClick={(e) => {
                                    e.preventDefault() 
                                    setIndexBread(index)}} 
                                    active={ index >= indexBreadcrum}
                                >
                                    {bread}
                                </BreadcrumbItem>
                            ))}
                        </Breadcrumb>
                    </div>
                    {indexBreadcrum === 0 && (
                        <div id="cars" style={{bottom: '-80px'}}>
                            <div className="container">
                                <Card className="p-4 mx-auto shadow bg-body rounded">
                                    <Card.Body>
                                        <Card.Title className="fw-bold">Detail Pesananmu</Card.Title>
                                        <Row>
                                            <Col>
                                                <label>Tipe Driver</label>
                                                <p className="mb-0 mt-1">{order.type_order === '0' ? 'Dengan Sopir' : 'Lepas Tangan'}</p>
                                            </Col>
                                            <Col>
                                                <label>Tanggal</label>
                                                <p className="mb-0 mt-1">{order.date}</p>
                                            </Col>
                                            <Col>
                                                <label>Waktu Jemput / Antar</label>
                                                <p className="mb-0 mt-1">{order.pickup_time}</p>
                                            </Col>
                                            <Col>
                                                <label>Jumlah Penumpang (opsional)</label>
                                                <p className="mb-0 mt-1">{order.passenger <= 0 ? '-' : order.passenger}</p>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </div>
                        </div>
                    )}
                </Container>
            </section>
            {indexBreadcrum === 0 && (
                <section className={indexBreadcrum === 0 ? 'mt-5 py-5' : 'py-5'}>
                    <Container>
                        <Row>
                            <Col xs={7}>
                                <Card className="p-4 shadow bg-body rounded">
                                    <Card.Body>
                                        <Card.Title style={{fontWeight: 'bold'}}>Pilih Bank Transfer</Card.Title>
                                        <Card.Text>
                                            <p>Kamu bisa membayar dengan transfer melalui ATM, Internet Banking atau Mobile Banking</p>
                                            <ListGroup variant="flush">
                                                <ListGroupItem action onClick={() => selectPayment('BCA')} active={payment === 'BCA'}><Button variant="outline-dark" style={{minWidth: '60px', fontSize: '12px'}}>BCA</Button> BCA Transfer</ListGroupItem>
                                                <ListGroupItem action onClick={() => selectPayment('BNI')}  active={payment === 'BNI'}><Button variant="outline-dark" style={{minWidth: '60px', fontSize: '12px'}}>BNI</Button> BNI Transfer</ListGroupItem>
                                                <ListGroupItem action  onClick={() => selectPayment('Mandiri')}  active={payment === 'Mandiri'}><Button variant="outline-dark" style={{minWidth: '60px', fontSize: '12px'}}>Mandiri</Button> Mandiri Transfer</ListGroupItem>
                                            </ListGroup>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col xs={5}>
                                <Card className="p-4 shadow bg-body rounded">
                                    <Card.Body>
                                        <Card.Title style={{fontWeight: 'bold'}}>{selectedCar.manufacture} {selectedCar.model}</Card.Title>
                                        <div className="d-flex gap-3">
                                            <p><FiUsers /> {selectedCar.capacity}</p>
                                            <p><FiSettings /> {selectedCar.transmission}</p>
                                            <p><FiCalendar /> Tahun {selectedCar.year}</p>
                                        </div>
                                        <Row className="mt-3 flex-column">
                                            <Col>
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <Button variant="ghost" onClick={() => setExpand(!expand)}>Total <FiChevronDown style={{transform: expand ? 'rotate(180deg)' : 'rotate(0)', transition: '.2s'}}/></Button>
                                                    <p><strong className="my-2 d-block">Rp. {selectedCar.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</strong></p>
                                                </div>
                                            </Col>
                                            {expand && (
                                                <Col style={{transform: expand ? 'translateX(-100)' : 'translateX(0)', transition: '.2s'}}>
                                                    <p><strong>Harga</strong></p>
                                                    <ul>
                                                        <Row>
                                                            <Col>
                                                                <li>1 Mobil {order.type_order === '0' && 'Dengan Sopir'}</li>
                                                            </Col>
                                                            <Col style={{textAlign: 'right'}}>
                                                                <span>Rp. {selectedCar.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</span>
                                                            </Col>
                                                        </Row>
                                                    </ul>
                                                    <p><strong>Biaya Lainnya</strong></p>
                                                    <ul>
                                                        <Row>
                                                            <Col>
                                                                <li>Pajak</li>
                                                            </Col>
                                                            <Col style={{textAlign: 'right'}}>
                                                                <span>Termasuk</span>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col>
                                                                <li>Biaya makan sopir</li>
                                                            </Col>
                                                            <Col style={{textAlign: 'right'}}>
                                                                <span>Termasuk</span>
                                                            </Col>
                                                        </Row>
                                                    </ul>
                                                    <p><strong>Belum Termasuk</strong></p>
                                                    <ul>
                                                        <li>Bensin</li>
                                                        <li>Tol dan parkir</li>
                                                    </ul>
                                                </Col>
                                            )}
                                            <hr />
                                            <Col>
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <p><strong>Total</strong></p>
                                                    <p><strong className="my-2 d-block">Rp. {selectedCar.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</strong></p>
                                                </div>
                                            </Col>
                                            <Col className="d-grid">
                                                <Button onClick={pay} variant="success" disabled={payment === ''}>Bayar</Button>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </section>
            )}

            {indexBreadcrum === 1 && (
                <section className="mt-1 py-3">
                    <Container>
                        <Row>
                            <Col xs={7}>
                                <Card className="p-4 shadow bg-body rounded">
                                    <Card.Body>
                                        <Row className="align-items-center">
                                            <Col xs={10}>
                                                <Card.Title style={{fontWeight: 'bold'}}>Selesaikan Pembayaran Sebelum</Card.Title>
                                                <Card.Text>{afterDay.locale('id').format('dddd, DD MMMM YYYY [jam] HH:mm:ss')}</Card.Text>
                                            </Col>
                                            <Col>
                                                <Card.Text>{timeBetween.hours()}:{timeBetween.minutes()}:{timeBetween.seconds()}</Card.Text>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                                <Card className="mt-4 p-4 shadow bg-body rounded">
                                    <Card.Body>
                                        <Card.Title><strong>Lanjutkan transfer ke</strong></Card.Title>
                                        <Card.Text>
                                            <Row>
                                                <Col>
                                                    <Button variant="outline-dark">{order.payment}</Button>
                                                </Col>
                                                <Col>
                                                    <p>{order.payment} Transfer <br /> a.n Binar Car Rental</p>
                                                </Col>
                                            </Row>
                                            <Form className="d-flex flex-column gap-3">
                                                <FormGroup>
                                                    <Form.Label>Nomor Rekening</Form.Label>
                                                    <Form.Control type="text" placeholder="xxxx-xxxx-xxxx" />
                                                </FormGroup>
                                                <FormGroup>
                                                    <Form.Label>Total Bayar</Form.Label>
                                                    <InputGroup>
                                                        <InputGroup.Text id="rp">Rp.</InputGroup.Text>
                                                        <Form.Control type="text" placeholder="Masukkan total bayar" aria-describedby="rp" />
                                                    </InputGroup>
                                                </FormGroup>
                                            </Form>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <Card className="mt-4 p-4 shadow bg-body rounded">
                                    <Card.Body>
                                        <Card.Title><strong>Instruksi Pembayaran</strong></Card.Title>
                                        <Card.Text>
                                        <Tabs
                                            activeKey={tabKey}
                                            onSelect={(k) => setTabKey(k)} 
                                        >
                                            <Tab eventKey="atm" title={'ATM ' + order.payment}>
                                                    <ul className="mt-2">
                                                        <li>
                                                        Tidak termasuk biaya makan sopir Rp 75.000/hari
                                                        </li>
                                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                        <li>Tidak termasuk akomodasi penginapan</li>
                                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                        <li>Tidak termasuk akomodasi penginapan</li>
                                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                        <li>Tidak termasuk akomodasi penginapan</li>
                                                    </ul>
                                                    
                                            </Tab>
                                            <Tab eventKey="m" title={'M-' + order.payment}>
                                                    <p>M {order.payment}</p>
                                            </Tab>
                                            <Tab eventKey="klik" title={ order.payment + ' Klik' }>
                                                    <p>{order.payment} Klik</p>
                                            </Tab>
                                            <Tab eventKey="banking" title={ 'Internet Banking' }>
                                                    <p>Internet Banking {order.payment}</p>
                                            </Tab>
                                        </Tabs>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                            <Col xs={5}>
                                <Card className="p-4 shadow bg-body rounded">
                                    <Card.Body>
                                        <Card.Text>
                                            <p>Klik konfirmasi pembayaran untuk mempercepat proses pengecekan</p>
                                            <Row>
                                                <Button variant="success">Konfirmasi Pembayaran</Button>
                                            </Row>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Container>
                </section>
            )}
        </>
    )
}

export default Payment
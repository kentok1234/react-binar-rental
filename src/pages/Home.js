import { Accordion, Button, Card, Carousel, Col, Container, Row } from "react-bootstrap";
import AccordionBody from "react-bootstrap/esm/AccordionBody";
import AccordionHeader from "react-bootstrap/esm/AccordionHeader";
import { FiCheckCircle } from "react-icons/fi";
import { Link } from "react-router-dom";
import Mercedes from "./mercedes.png"
import Service from "./img_service.png"

function Home() {
    return (
        <>
            <section className="bg-main vh-100 position-relative overflow-hidden" style={{paddingTop: '4em'}}>
                <Container className="h-full">
                <Row className="h-full">
                    <Col xs={12} lg={6} className="d-flex align-items-lg-center">
                    <div className="text-black">
                        <h1 className="fw-bold mb-4 hero-title">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                        <p className="mb-4 hero-paragraph">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        <Button as={Link} to="/cars" className="fw-bold bg-lime-green400 text-white">Mulai Sewa Mobil</Button>
                    </div>  
                    </Col>
                    <Col xs={12} lg={6} className="d-flex align-items-end">
                    <div id="car-container" className="bg-dark-blue400 rounded-top-left-10">
                        <img src={Mercedes} alt="Mecedes Toyota" id="car-image" />
                    </div>
                    </Col>
                </Row>
                </Container>
            </section>
            <section className="py-5" id="service">
            <Container className="d-flex flex-column-reverse flex-lg-row py-5">
                <div className="px-5 position-relative d-flex justify-content-center justify-content-lg-end  align-items-center flex-grow-1 mt-sm-5 mt-lg-0 order-1 order-lg-0 mb-3 mb-lg-0">
                    <img src={Service} alt="Woman Service Image" />
                </div>
                <div className="w-half w-lg-full text-black ps-lg-4 mt-5 mt-lg-0">
                <h2 className="fw-bold fs-4 mb-4">Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                <ul className="list-style-check d-flex flex-column gap-4">
                    <li><FiCheckCircle /> Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                    <li><FiCheckCircle /> Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                    <li><FiCheckCircle /> Sewa Mobil Jangka Panjang Bulanan</li>
                    <li><FiCheckCircle /> Gratis Antar - Jemput Mobil di Bandara</li>
                    <li><FiCheckCircle /> Layanan Airport Transfer / Drop In Out</li>
                </ul>
                </div>
            </Container>
            </section>

            <section id="whyus" className="py-5">
            <Container>
                <header className="text-black text-center text-lg-start mb-5">
                <h2 className="fw-bold mb-3">Why Us?</h2>
                <p>Mengapa harus pilih Binar Car Rental?</p>
                </header>
                <Row lg={4} md={2} xs={1} className="gy-3">
                <Col>
                    <Card className="border-secondary mb-3">
                    <Card.Body>
                        <img src="images/icons/icon_complete.png" alt="Icon Complete" className="mb-3" />
                        <h3 className="card-title mb-3 fs-5 fw-bold">Mobil Lengkap</h3>
                        <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                    </Card.Body>
                    </Card>
                </Col>
                <Col>
                    <Card className="border-secondary mb-3">
                    <div className="card-body">
                        <img src="images/icons/icon_price.png" alt="Icon Price" className="mb-3" />
                        <h3 className="card-title mb-3 fs-5 fw-bold">Harga Murah</h3>
                        <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                    </div>
                    </Card>
                </Col>
                <Col>
                    <div className="card border-secondary mb-3">
                    <div className="card-body">
                        <img src="images/icons/icon_24hrs.png" alt="Icon 24" className="mb-3" />
                        <h3 className="card-title mb-3 fs-5 fw-bold">Layanan 24 Jam</h3>
                        <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                    </div>
                    </div>
                </Col>
                <Col>
                    <div className="card border-secondary mb-3">
                    <div className="card-body">
                        <img src="images/icons/icon_professional.png" alt="Icon Profesional" className="mb-3" />
                        <h3 className="card-title mb-3 fs-5 fw-bold">Sopir Profesional</h3>
                        <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                    </div>
                    </div>
                </Col>
                </Row>
            </Container>
            </section>

            <section id="testimonial" className="py-5">
            <Container>
                <header className="text-black mb-5 text-center">
                <h2 className="fw-bold mb-3">Testimonial</h2>
                <p>Berbagai review positif dari para pelanggan kami</p>
                </header>

                <Carousel >
                <Carousel.Item>
                    <Card className="bg-main">
                    <Card.Body className="d-flex justify-content-between align-items-center">
                        <img style={{clipPath: 'circle(50%)', width: '80px'}} src="https://images.unsplash.com/photo-1552374196-c4e7ffc6e126?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387" alt="Icon Profesional" />
                        <div className="ms-4">
                        <p>⭐⭐⭐⭐⭐</p>
                        <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                        <p className="fw-bold">John Dee 32, Bromo</p>
                        </div>
                    </Card.Body>
                    </Card>
                </Carousel.Item>
                <Carousel.Item>
                    <Card className="bg-main">
                    <Card.Body className="d-flex justify-content-between align-items-center">
                        <img style={{clipPath: 'circle(50%)', width: '80px'}} src="https://images.unsplash.com/photo-1504199367641-aba8151af406?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387" alt="Icon Profesional" />
                        <div className="ms-4">
                        <p>⭐⭐⭐⭐⭐</p>
                        <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                        <p className="fw-bold">John Dee 32, Bromo</p>
                        </div>
                    </Card.Body>
                    </Card>
                </Carousel.Item>
                <Carousel.Item>
                    <Card className="bg-main">
                    <Card.Body className="d-flex justify-content-between align-items-center">
                        <img style={{clipPath: 'circle(50%)', width: '80px'}} src="https://images.unsplash.com/photo-1570295999919-56ceb5ecca61?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580" />
                        <div className="ms-4">
                        <p>⭐⭐⭐⭐⭐</p>
                        <p className="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                        <p className="fw-bold">John Dee 32, Bromo</p>
                        </div>
                    </Card.Body>
                    </Card>
                </Carousel.Item>
                </Carousel>

                <div className="mt-5 bg-dark-blue400 text-center text-white py-5 rounded">
                <p className="fw-bold fs-5">Sewa Mobil di (Lokasimu) Sekarang</p>
                <p className="w-half mx-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                <Button as={Link} to="/cars" className="fw-bold bg-lime-green400 text-white">Mulai Sewa Mobil</Button>
                </div>
            </Container>
            </section>

            <section id="faq" className="py-5">
            <Container>
                <Row>
                <Col className="text-center text-lg-start">
                    <h2 className="fw-bold">Frequently Asked Question</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </Col>

                <Col lg={7}>
                    <Accordion defaultActiveKey={"0"}>
                    <Accordion.Item eventKey="0">
                        <AccordionHeader>Apa saja syarat yang dibutuhkan?</AccordionHeader>
                        <AccordionBody>
                            <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classNamees that we use to style each element. These classNamees control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                        </AccordionBody>
                    </Accordion.Item>
                    <Accordion.Item eventKey="1">
                        <AccordionHeader>Berapa hari minimal sewa mobil lepas kunci?</AccordionHeader>
                        <AccordionBody>
                            <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classNamees that we use to style each element. These classNamees control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.                      
                        </AccordionBody>
                    </Accordion.Item>
                    <Accordion.Item eventKey="2">
                        <AccordionHeader>Berapa hari sebelumnya sabaiknya booking sewa mobil?</AccordionHeader>
                        <AccordionBody>
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classNamees that we use to style each element. These classNamees control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                        </AccordionBody>
                    </Accordion.Item>
                    <Accordion.Item eventKey="3">
                        <AccordionHeader>Apakah Ada biaya antar-jemput?</AccordionHeader>
                        <AccordionBody>
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classNamees that we use to style each element. These classNamees control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                        </AccordionBody>
                    </Accordion.Item>
                    <Accordion.Item eventKey="4">
                        <AccordionHeader>Bagaimana jika terjadi kecelakaan</AccordionHeader>
                        <AccordionBody>
                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classNamees that we use to style each element. These classNamees control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                        </AccordionBody>
                    </Accordion.Item>
                    </Accordion>
                </Col>
                </Row>
            </Container>
            </section>
        </>
    )
}

export default Home
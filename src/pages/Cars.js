import axios from "axios"
import moment from "moment"
import { useEffect, useState } from "react"
import { Container, Col, Row, Button, Form, Card } from "react-bootstrap"
import Mercedes from "./mercedes.png"
import BeepBeep from "./img-BeepBeep.png"
import { FiUsers, FiSettings, FiCalendar } from "react-icons/fi";
import { useDispatch, useSelector } from "react-redux"
import { fetchCars, getAllCars } from "../features/cars/carsSlice"
import { getSelectedCar, selectCarAction } from "../features/cars/carSlice"
import { Link } from "react-router-dom"
import { orderAction } from "../features/cars/orderSlice"

function Cars() {
    const [driverType, setDriverType] = useState('')
    const [date, setDate] = useState('')
    const [pickupTime, setPickuTime] = useState('')
    const [passenger, setPassenger] = useState(0)

    const [validated, setValidated] = useState(false)
    const [filteredCars, setFilteredCars] = useState([])

    const dispatch = useDispatch()
    const cars = useSelector(getAllCars)
    const car = useSelector(getSelectedCar)
    const carsStatus = useSelector((state) => state.cars.status)
    const error = useSelector((state) => state.cars.error)

    const [selectedCar, setSelectedCar] = useState({})
    const [carFounded, setCarFounded] = useState(true)
    const [isSelectedCar, setIsSelectedCar] = useState(false)

    useEffect(() => {
        if (carsStatus === 'idle') {
            dispatch(fetchCars())
        }
    }, [carsStatus, dispatch, isSelectedCar, carFounded])
    
    const submitOrder = async (event) => {
        event.preventDefault();
        const form = event.currentTarget

        if (form.checkValidity() === false) {
            event.stopPropagation();
            setValidated(true);
            return
        }

        setIsSelectedCar(false)

        const rentalDate = date + ' ' + pickupTime
        
        const filterCars = cars.filter(car => (
            car.capacity >= passenger && moment(car.availableAt).unix() >= moment(rentalDate).unix()
        ))

        if (filterCars.length > 0) {
            setCarFounded(true)
            setFilteredCars(filterCars)
        } else {
            setCarFounded(false)
        }

        
    }

    const selectCar = (car) => {
        setIsSelectedCar(true)
        setCarFounded(false)
        setSelectedCar(car)
        dispatch(orderAction.updateOrder({
            type_driver: driverType,
            date: date,
            pickup_time: pickupTime,
            passenger: passenger
        }))
        dispatch(selectCarAction.setSelectCar(car))
    }

    return (
        <>
            <section className="bg-main vh-100 position-relative overflow-hidden" style={{paddingTop: '4em'}}>
                <Container className="h-full">
                    <Row className="h-full">
                        <Col xs={12} lg={6} className="d-flex align-items-lg-center">
                            <div className="text-black">
                                <h1 className="fw-bold mb-4 hero-title">Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                                <p className="mb-4 hero-paragraph">Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                            </div>  
                        </Col>
                        <Col xs={12} lg={6} className="d-flex align-items-end">
                            <div id="car-container" className="bg-dark-blue400 rounded-top-left-10">
                                <img src={Mercedes} alt="Mecedes Toyota" id="car-image" />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </section>
            <section id="cars">
                <div className="container">
                    <div className="card p-4 mx-auto shadow bg-body rounded">
                        <Form onSubmit={submitOrder} validated={validated} noValidate >
                            <Row>
                                <Col lg={2} md={12}>
                                    <Form.Group>
                                        <Form.Label htmlFor="selectCars">Tipe Driver</Form.Label>
                                        <Form.Select id="selectCars" className="form-select" aria-label="Tipe Driver" onChange={(e) => setDriverType(e.target.value)} placeholder="Pilih Tipe Driver" required>
                                            <option selected disabled>Pilih Tipe Driver</option>
                                            <option value="0">Dengan Supir</option>
                                            <option value="1">Lepas Kunci</option>
                                        </Form.Select>
                                        <Form.Control.Feedback type="invalid">
                                            Pilih tipe driver.
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Col>
                                <Col lg={2} md={12}>
                                    <Form.Group>
                                        <Form.Label htmlFor="date">Tanggal</Form.Label>
                                        <Form.Control type="date" name="date" id="date" onChange={(e) => setDate(e.target.value)} required />
                                        <Form.Control.Feedback type="invalid">
                                           Masukkan tanggal.
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Col>
                                <Col lg={3} md={12}>
                                    <Form.Group>
                                        <Form.Label htmlFor="time">Waktu Jemput/Ambil</Form.Label>
                                        <Form.Control type="time" name="time" id="time" onChange={(e) => setPickuTime(e.target.value)} required />
                                        <Form.Control.Feedback type="invalid">
                                            Isi waktu jemput.
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Col>
                                <Col lg={3} md={12}>
                                    <Form.Group>
                                        <Form.Label htmlFor="capacity">Jumlah Penumpang (Optional)</Form.Label>
                                        <Form.Control  type="number" name="capacity" id="capacity" placeholder="Jumlah Penumpang" onChange={(e) => setPassenger(e.target.value)} />
                                        <Form.Control.Feedback type="invalid">
                                            Masukkan jumlah penumpang dengan benar.
                                        </Form.Control.Feedback>
                                    </Form.Group>
                                </Col>
                                <Col lg={2} md={12}>
                                    <Form.Group>
                                        <Form.Label style={{visibility: "hidden"}}>Tombol</Form.Label>
                                        <Button type="submit" className="fw-bold" id="load-btn">Cari Mobil</Button>
                                    </Form.Group>

                                </Col>
                            </Row>
                        </Form>
                    </div>
                </div>
            </section>

            <section className='mt-5 py-5'>
                <Container>
                    {filteredCars.length > 0 && carFounded && (
                        <Row lg={3} className="gy-3">
                            {filteredCars.map(car => (
                                <Col key={car.id}>
                                    <Card style={{height: '100%'}}>
                                        <Card.Img variant="top" src={car.image} style={{height: '222px', objectFit: 'cover'}} />
                                        <Card.Body className="d-flex flex-column justify-content-between">
                                            <Card.Text>
                                                <Card.Title>{car.manufacture} {car.model}</Card.Title>
                                                <strong className="my-2 d-block">Rp. {car.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</strong>
                                                <p>{car.description}</p>
                                                <p><FiUsers /> {car.capacity}</p>
                                                <p><FiSettings /> {car.transmission}</p>
                                                <p><FiCalendar /> Tahun {car.year}</p>
                                            </Card.Text>
                                            <div className="d-grid">
                                                <Button variant={(car.available) ? 'success' : 'danger'} style={{cursor: !car.available && 'not-allowed'}} disabled={!car.available} onClick={() => selectCar(car)}>Pilih Mobil</Button>
                                            </div>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            ))}
                        </Row>
                    ) }

                    {!carFounded && !filteredCars.length > 0 && (
                        <div className="d-flex align-items-center justify-content-center flex-column">
                            <img src={BeepBeep} alt="Empty Cars" style={{maxWidth: '200px'}}/>
                            <p><strong>Mohon maaf mobil yang dicari tidak ada</strong></p>
                        </div>
                    )}
                    
                    {isSelectedCar && (
                        <Row>
                            <Col xs={7}>
                                <Card className="p-4">
                                    <Card.Body>
                                        <Card.Title style={{fontWeight: 'bold'}}>Tentang Paket</Card.Title>
                                        <Card.Text>
                                            <p>Include</p>
                                            <ul>
                                                <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                                                <li>Sudah termasuk bensin selama 12 jam</li>
                                                <li>Sudah termasuk Tiket Wisata</li>
                                                <li>Sudah termasuk pajak</li>
                                            </ul>
                                            <p>Exclude</p>
                                            <ul>
                                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                <li>Tidak termasuk akomodasi penginapan</li>
                                            </ul>
                                            <p><strong>Refund, Reschedule, Overtime</strong></p>
                                            <ul>
                                                <li> Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                <li>Tidak termasuk akomodasi penginapan</li>
                                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                <li>Tidak termasuk akomodasi penginapan</li>
                                                <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                <li>Tidak termasuk akomodasi penginapan</li>
                                            </ul>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                                <div className="mt-4 d-flex justify-content-end">
                                    <Button as={Link} to='/payment' variant="success">Lanjutkan Pembayaran</Button>
                                </div>
                            </Col>
                            <Col xs={5}>
                                <Card className="p-4">
                                    <Card.Img variant="top" src={selectedCar.image} style={{height: '222px', objectFit: 'cover'}} />
                                    <Card.Body>
                                        <Card.Title>{selectedCar.manufacture} {selectedCar.model}</Card.Title>
                                        <div className="d-flex gap-3">
                                            <p><FiUsers /> {selectedCar.capacity}</p>
                                            <p><FiSettings /> {selectedCar.transmission}</p>
                                            <p><FiCalendar /> Tahun {selectedCar.year}</p>
                                        </div>
                                        <Row className="mt-3 flex-column">
                                            <Col>
                                                <div className="d-flex justify-content-between align-items-center">
                                                    <p>Total</p>
                                                    <p><strong className="my-2 d-block">Rp. {selectedCar.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</strong></p>
                                                </div>
                                            </Col>
                                            <Col className="d-grid">
                                                <Button as={Link} to='/payment' variant="success">Lanjutkan Pembayaran</Button>
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    )}
                </Container>
            </section>
        </>
    )
}

export default Cars
import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    data: {},
}

const orderSlice = createSlice({
    name: 'order',
    initialState,
    reducers: {
        updateOrder(state, action) {
            const {payload: order} = action
            Object.assign(state.data, order)
        },
    },
})

export const getOrder = state => state.order.data
export const orderAction = orderSlice.actions
export default orderSlice.reducer
import {createAsyncThunk, createSlice} from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
    data: [],
    status: 'idle',
    error: null
}

export const fetchCars = createAsyncThunk('cars/fetchCars', async () => {
    const response = await axios.get('/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
    return response.data
})

const carsSlice = createSlice({
    name: 'cars',
    initialState,
    reducers: {},
    extraReducers: {
        [fetchCars.pending]: (state, action) => {
            state.status = 'loading'
        },
        [fetchCars.fulfilled]: (state, action) => {
            state.status = 'success'
            state.data = action.payload
        },
        [fetchCars.rejected]: (state, action) => {
            state.status = 'failed'
            state.error = action.error.message
        }
    }
})

export const getAllCars = state => state.cars.data
export default carsSlice.reducer
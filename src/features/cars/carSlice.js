import {createSlice} from '@reduxjs/toolkit'

const initialState = {
    data: {},
}

const carSlice = createSlice({
    name: 'car',
    initialState,
    reducers: {
        setSelectCar(state, action) {
            const {payload: car} = action
            state.data = car
        },
    },
})

export const getSelectedCar = state => state.car.data
export const selectCarAction = carSlice.actions
export default carSlice.reducer